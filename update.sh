#!/bin/bash
# WordPress update script for Fixrs Docker4WordPress

# Core
echo '====================================================='
echo 'Updating the WordPress Core'
echo ''
echo ''

echo 'Starting the db and waiting for the db to be ready'
docker-compose up mariadb
sleep 10

core_version=$(docker-compose run --rm php wp core version --allow-root)
core_version=${core_version%?}
core_updated_version=$(docker-compose run --rm php wp core check-update --field=version --allow-root --format=csv)
core_updated_version=${core_updated_version%?}
docker-compose run --rm php wp core update --allow-root &&
    git add -A wp &&
    git commit -m "Update WordPress Core $core_version => $core_updated_version"

# Plugins
echo ''
echo ''
echo '====================================================='
echo 'Updating the Plugins'

for line in $(docker-compose run --rm php wp plugin list --update=available --fields=name,version,update_version --allow-root --format=csv);
do
	plugin=$(echo $line | cut -f1 -d,)
	curr_version=$(echo $line | cut -f2 -d,)
	update_version=$(echo $line | cut -f3 -d,)
	echo ''
	echo "Updating $plugin ================================"
	echo ''
	docker-compose run --rm php wp plugin update $plugin --allow-root &&
	git add -A wp/wp-content/plugins/$plugin &&
	git commit -m "Update Plugin '$plugin' $curr_version => $update_version"
done

#Translation
echo ''
echo ''
echo 'Updating the Translations'
echo ''

docker-compose run --rm php wp language plugin update --all --allow-root &&
    git add -A wp/wp-content/languages/plugins &&
    git commit -m 'Update Languages'
