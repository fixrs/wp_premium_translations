== Fixrs Core ==


== Description ==

Fonctionalités de support et de personnalisation développés par Fixrs.

== Changelog ==
= 0.0.4 =
Ajout White Label
Image preprocessor 
Authentification limitation
Security precautions
User takeover feature 

= 0.0.3 =
Ajout de la gestion de fichier ACF JSON
Ajout du role Client Admin
Ajout du deactivation hook

= 0.0.2 =
Personnalisation des widgets du tableau de bord
Ajout de fonctions de déboggage

= 0.0.1 =
Création initial de l’extension