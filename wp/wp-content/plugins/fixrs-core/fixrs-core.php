<?php

/**
 * Plugin Name: Fixrs Core
 * Plugin URI: https://Fixrs.ca
 * Description: Support and customization features
 * Version: 0.0.4
 * Author: Charles St-Pierre @fixrs
 * Author URI: https://fixrs.ca
 * Text Domain: fixrs
 * Domain Path: /lang
 */
/*
  Changelog
  v.0.0.4
 * Ajout White Label
 * Image preprocessor 
 * Authentification limitation
 * Security precautions
 * User takeover feature 
 * 
 * 
  v.0.0.3
 * Ajout de la gestion de fichier ACF JSON
 * Ajout du role Client Admin
 * Ajout du deactivation hook

  v.0.0.2
 * Personnalisation des widgets du tableau de bord
 * Ajout de fonctions de déboggage

  v.0.0.1
 * Création initial de l’extension

 */

namespace Fixrs;

// Exit if accessed directly
if (!defined('ABSPATH')) {
    exit;
}
define('FIXRSCORE_VERSION', '0.0.4');
define('FIXRSCORE_URL', plugin_dir_url(__FILE__));
define('FIXRSCORE_PATH', dirname(plugin_basename(__FILE__)));

require_once 'inc/default-options.php';

require_once 'inc/functions.php';
require_once 'inc/security.php';
require_once 'inc/upload-processor.php';
require_once 'inc/dashboardwidget.php';
require_once 'inc/acf-json-manager.php';
require_once 'inc/log-as-user.php';

/**
 * Initialise l’extension
 * 
 * @return nada
 */
function init() {
    // loading text domain
    load_plugin_textdomain('fixrs', false, FIXRSCORE_PATH . '/lang/');
    return;
}

add_action('plugins_loaded', 'Fixrs\init', 1, 1);

/**
 * 
 * @return type
 */
function admin_enqueue() {
    wp_enqueue_style('fixrs-core', FIXRSCORE_URL . (SCRIPT_DEBUG ? '/dev/css/admin.css' : '/css/admin.min.css'), array(), FIXRSCORE_VERSION);
    return;
}

add_action('admin_enqueue_scripts', 'Fixrs\admin_enqueue', 1, 1);

/**
 * 
 * @return void
 */
function activate_plugin() {
    do_action('Fixrs/activate_plugin');
    return;
}

register_activation_hook(__FILE__, 'Fixrs\activate_plugin');

/**
 * 
 * @return void
 */
function deactivate_plugin() {
    do_action('Fixrs/deactivate_plugin');
    return;
}

register_deactivation_hook(__FILE__, 'Fixrs\deactivate_plugin');
