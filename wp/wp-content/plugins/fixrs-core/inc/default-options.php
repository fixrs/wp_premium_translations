<?php

/*
  Copyright (C) 2020 fixrs

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
namespace Fixrs\defaults;

if (!defined('ABSPATH')) {
    die('Invalid request.');
}

/**
 * Définis les constantes par défaut nécessaire à l’extension.
 * 
 * Peut-être overwritten dans wp-config.php, dans l’extension client ou dans le thème
 * 
 * @return void
 */
function load_options() {

    #if (!defined('FIXRS_CONSTANT')) {
    #    define('FIXRS_CONSTANT', false);
    #}
    if (!defined('FIXRS_ACTIVATE_DEMO')) {
        define('FIXRS_ACTIVATE_DEMO', false);
    }
    
    if (!defined('FIXRS_WHITELABEL')) {
        define('FIXRS_WHITELABEL',true);
    }
    
    if (!defined('FIXRS_DO_RSS')) {
        define('FIXRS_DO_RSS',true);
    }
    
    if (!defined('FIXRS_DO_SECURITY')) {
        define('FIXRS_DO_SECURITY',true);
    }
    
    if (!defined('FIXRS_DISABLE_XMLRPC')) {
        define('FIXRS_DISABLE_XMLRPC',true);
    }
    
    if (!defined('FIXRS_SECURITY_MAX_BLACKLIST')) {
        define('FIXRS_SECURITY_MAX_BLACKLIST',50);
    }
    if (!defined('FIXRS_SECURITY_MAX_404')) {
        define('FIXRS_SECURITY_MAX_404',100);
    }
    if (!defined('FIXRS_SECURITY_LOGIN_TO_WHITELIST')) {
        define('FIXRS_SECURITY_LOGIN_TO_WHITELIST',20);
    }
    
    if (!defined('FIXRS_IMAGE_MAX_WIDTH')) {
        define('FIXRS_IMAGE_MAX_WIDTH',5120);
    }
    if (!defined('FIXRS_IMAGE_MAX_HEIGHT')) {
        define('FIXRS_IMAGE_MAX_HEIGHT',5120);
    }
    if (!defined('FIXRS_IMAGE_QUALITY')) {
        define('FIXRS_IMAGE_QUALITY',90);
    }
    return;
}

add_action('plugins_loaded', 'Fixrs\defaults\load_options', 10, 1);
