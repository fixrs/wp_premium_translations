<?php

namespace Fixrs\security;

// Exit if accessed directly
if (!defined('ABSPATH')) {
    exit;
}

/**
 * Basic security actions and filters
 * 
 * Use basic actions and filters to change WP innerworking to make it more secure.
 * 
 * - Remove metatag generator to hide that the site is ran by WordPress
 * 
 * @since 1.0.0
 */
function init() {
    if (!FIXRS_DO_RSS) {
        remove_action('wp_head', 'feed_links', 2);
        remove_action('wp_head', 'feed_links_extra', 3);
    }
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wlwmanifest_link');
    remove_action('wp_head', 'index_rel_link');
    remove_action('wp_head', 'parent_post_rel_link', 10, 0);
    remove_action('wp_head', 'start_post_rel_link', 10, 0);
    remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
    remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
    remove_action('wp_head', 'wp_generator', 10);
    if (isset($GLOBALS['sitepress'])) {
        remove_action('wp_head', array($GLOBALS['sitepress'], 'meta_generator_tag'), 10);
    }


    /**
     * Secure WP by disabling XML-RPC usage
     */
    if (FIXRS_DISABLE_XMLRPC) {
        add_filter('xmlrpc_methods', function ($methods) {
            unset($methods['system.multicall']);
            unset($methods['system.listMethods']);
            unset($methods['system.getCapabilities']);
            unset($methods['pingback.extensions.getPingbacks']);
            unset($methods['pingback.ping']);
            return $methods;
        });

        add_action('wp', function () {
            header_remove('X-Pingback');
        }, 9999);
    }

    /**
     * Add filters and actions only if IP is not whitelisted
     */
    if (FIXRS_DO_SECURITY && !is_whitelist()) {
        add_filter('authenticate', 'Fixrs\security\authenticate_limit_access', 50, 3);
        add_action('wp', 'Fixrs\security\security_404', 100);
        add_action('plugins_loaded', 'Fixrs\security\apply_gandalf_protocol', 5);
    }
}

add_action('plugins_loaded', 'Fixrs\security\init');

/**
 * Update User_ID on activate
 * 
 * On plugin activation, changes USER_IDs to high values
 * 
 * @since 1.0.0
 * @global object $wpdb
 */
function reindex_user_id() {
    global $wpdb;

    // if user id 1 doesn't exist, its probably been done before.
    $count = $wpdb->get_var("SELECT COUNT(id) FROM $wpdb->users WHERE id=1;");
    if ($count == 1) { // id 1 exists
        // choose random id for admin account (id=1)
        $admin_id = rand(1000, 2000);
        // update tables to increase 

        $last_autoincrement = $wpdb->get_var("SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES WHERE table_name = '$wpdb->users' ");

        // choose random auto increment
        $step = rand(2000, 4000);
        $new_autoincrement = $step + $last_autoincrement;
        $zero_query = $wpdb->query("ALTER TABLE $wpdb->users AUTO_INCREMENT = $new_autoincrement;");

        // increase all user ids by $new_autoincrement;
        $first_query = $wpdb->query("UPDATE $wpdb->users AS u "
                . "LEFT JOIN $wpdb->usermeta AS m ON u.ID=m.user_id "
                . "LEFT JOIN $wpdb->posts AS p ON u.ID=p.post_author "
                . "LEFT JOIN $wpdb->comments AS c ON u.ID = c.user_id "
                . "SET u.ID = u.ID + $step, "
                . "m.user_id = m.user_id + $step, "
                . "p.post_author = p.post_author + $step, "
                . "c.user_id = c.user_id + $step "
                . "WHERE u.ID > 1");

        $second_query = $wpdb->query("UPDATE $wpdb->users AS u "
                . "LEFT JOIN $wpdb->usermeta AS m ON u.ID=m.user_id "
                . "LEFT JOIN $wpdb->posts AS p ON u.ID=p.post_author "
                . "LEFT JOIN $wpdb->comments AS c ON u.ID = c.user_id "
                . "SET u.ID = $admin_id, "
                . "m.user_id = $admin_id, "
                . "p.post_author = $admin_id, "
                . "c.user_id = $admin_id "
                . "WHERE u.ID = 1;");
        
        return true;
    }else{
        return false;
    }
}


/**
 * Update user nicename
 * 
 * Create a unique nicename for user, different form user_login.
 * 
 * @since 1.0.0
 * @global object $wpdb
 * @param int $user_id
 * @return void
 * @uses filter profile_update (saving profile)
 * @uses filter user_register (saving new profile)
 */
function update_nicename_from_nickname($user_id) {
    global $wpdb;
    $user = get_user_by('id', $user_id);

    if (strtolower($user->user_nicename) !== strtolower($user->user_login)) {
        // nicename is different
        return;
    } elseif ($user->nickname !== $user->user_login) {
        $new_nicename = sanitize_title($user->nickname);
    } elseif ($user->display_name !== $user->user_login) {
        $new_nicename = sanitize_title($user->display_name);
    } else {
        $new_nicename = 'user' . time();
    }
    if ($new_nicename) {
        $wpdb->query($wpdb->prepare(
                        "UPDATE $wpdb->users "
                        . "SET user_nicename='%s' "
                        . "WHERE ID=%d;", array(
                    $new_nicename,
                    $user_id
                        )
        ));
    }
}

add_action('profile_update', 'Fixrs\security\update_nicename_from_nickname', 10, 2);
add_action('user_register', 'Fixrs\security\update_nicename_from_nickname', 10, 2);

/**
 * INTERFACE    
 */

/**
 * Add security interface
 * 
 * Add the security inteface to Settings
 * 
 * @since 1.0.2
 */
function add_security_interface() {
    add_options_page(__('Security', 'fixrs'), __('Security', 'fixrs'), 'manage_options', 'fixrs-security', 'Fixrs\security\output_security_interface');
}

add_action('admin_menu', 'Fixrs\security\add_security_interface');

/**
 * Enqueue admin security CSS and JS
 */
function security_enqueue_head($hook) {
    if ($hook == 'settings_page_fixrs-security') {
        wp_enqueue_script('fixrs-security', FIXRSCORE_URL . 'js/admin-security.js', array('jquery'), FIXRSCORE_VERSION, true);
        wp_enqueue_style('fixrs-security', FIXRSCORE_URL . 'css/admin-security.css');
    }
}

add_action('admin_enqueue_scripts', 'Fixrs\security\security_enqueue_head');

/**
 * 
 */
function search_ip_blacklist() {

    if (!current_user_can('manage_options')) {
        wp_send_json_error('-1');
    }

    $response = array();

    $ip = filter_input(INPUT_POST, 'ip', FILTER_VALIDATE_IP);
    if (empty($ip)) {
        $response['code'] = 'invalid';
        $response['message'] = __('The IP is invalid.', 'fixrs');
    } else {
        $blacklist = get_option('_blacklist' . get_ip($ip));
        if (false === $blacklist) {
            $response['code'] = 'missing';
            $response['message'] = __('The IP is not on the blacklist.', 'fixrs');
        } else {
            $response['code'] = 'found';
            $response['message'] = __('The IP is on the blacklist.', 'fixrs');
        }
    }
    wp_send_json_success($response);
}

add_action('wp_ajax_search_ip_blacklist', 'Fixrs\security\search_ip_blacklist');

/**
 * Output Security interface
 * 
 * @since 1.0.2
 * @global object $wpdb
 */
function output_security_interface() {
    global $wpdb;

    $current_user = wp_get_current_user();
    $show_advanced_ui = false !== strpos($current_user->user_email, '@fixrs');

    if ($show_advanced_ui) {

        // reindex ONLY if default WP install is in play. 
        $user_id_1_exists = false;
        $nb_of_tables = (int) $wpdb->get_var(sprintf("SELECT count(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '%s'", $wpdb->dbname));
        if (12 === $nb_of_tables) {
            $user_id_1_exists = (int) $wpdb->get_var("SELECT COUNT(id) FROM $wpdb->users WHERE id=1;");
        }

        // check for compromized usernames
        $users_with_compromized_usernames = $wpdb->get_col("SELECT ID FROM $wpdb->users WHERE BINARY user_nicename = user_login;");

    }

    // submitted form
    if (wp_verify_nonce(filter_input(INPUT_POST, '_wpnonce'), 'fixrs-security-interface')) {

        $what = filter_input(INPUT_POST, 'what');

        switch ($what) {

            case 'reindex_user_id':
                $test = reindex_user_id();
                if ($test){
                    $success = __('All IDs were reindexed.','fixrs');
                }else{
                    $error = __('There were no ID to reindex.','fixrs');
                }
                
                break;
            case 'rename_user_nicenames':

                if (!empty($users_with_compromized_usernames)) {
                    
                    // create clean nicenames
                    
                    $user_repair = [];
                    
                    foreach ($users_with_compromized_usernames as $uid) {
                        $uid = (int) $uid;
                        $user = get_user_by('id', $uid);
                        
                        $sanitize_nickname = sanitize_title($user->nickname);
                        $sanitize_display_name = sanitize_title($user->display_name);
                        $sanitize_fullname = sanitize_title($user->first_name.'-'.$user->last_name);
                        $random = 'user-' . sha1($user->user_login);
                        
                        if ($sanitize_nickname !== $user->user_login) {
                            $new_nicename = $sanitize_nickname;
                        } elseif ($sanitize_display_name !== $user->user_login) {
                            $new_nicename = $sanitize_display_name;
                        } elseif ( $sanitize_fullname !== $user->user_login) {
                            $new_nicename = $sanitize_fullname;
                        } else {
                            $new_nicename = $random;
                        }
                        
                        $user_repair[] = "($uid, '$new_nicename')" ;
                        
                    }

                    // big ass query to rename 
                    $fix_query = "INSERT INTO $wpdb->users (`ID`,`user_nicename`) VALUES ";
                    $fix_query.= implode(', ',$user_repair);
                    $fix_query.= " ON DUPLICATE KEY UPDATE `user_nicename` = VALUES(`user_nicename`);";
                    
                    // run the query
                    $wpdb->query($fix_query);
                    
                    $n = count($users_with_compromized_usernames);
                    $success = sprintf( _n('One user secured.','%s users secured.',$n,'fixrs'), $n);
                    
                     $users_with_compromized_usernames = [];
               }
                break;
            case 'add_my_ip_whitelist':
                if (false !== ( $new_whitelist = filter_input(INPUT_POST, 'my_ip', FILTER_VALIDATE_IP))) {
                    add_to_whitelist($new_whitelist);
                    $success = __('IP added to the whitelist.','fixrs');
                }
                break;
            case 'add_ip_whitelist':
                if (false !== ( $new_whitelist = filter_input(INPUT_POST, 'add_ip_whitelist', FILTER_VALIDATE_IP))) {
                    add_to_whitelist($new_whitelist);
                    $success = __('IP added to the whitelist.','fixrs');
                }
                break;
            case 'add_ip_blacklist':
                if (false !== ( $new_blacklist = filter_input(INPUT_POST, 'add_ip_blacklist', FILTER_VALIDATE_IP))) {
                    add_option('_blacklist' . get_ip($new_blacklist), 1);
                    delete_option('_whitelist' . get_ip($new_blacklist));
                    $success = __('IP added to the blacklist.','fixrs');
                }
                break;
            case 'delete_ip_blacklist':
                if (false !== ( $the_ip = filter_input(INPUT_POST, 'search_ip_blacklist', FILTER_VALIDATE_IP))) {
                    delete_option('_blacklist' . get_ip($the_ip));
                    gandalf_protocol_remove_ip($the_ip);
                    $success = __('IP removed from to the blacklist.','fixrs');
                }
                break;
            default:
                // we are removing something
                $the_list = filter_input(INPUT_POST, 'remove_list');
                $the_ip = filter_input(INPUT_POST, 'remove_ip', FILTER_VALIDATE_IP);

                if (in_array($the_list, array('black', 'white')) && $the_list && $the_ip) {
                    delete_option('_' . $the_list . 'list' . get_ip($the_ip));
                    gandalf_protocol_remove_ip($the_ip);
                    
                    $success = __('IP removed.','fixrs');
                }

                break;
        }
    }

    if ($show_advanced_ui) {
        if (!$user_id_1_exists && empty($users_with_compromized_usernames)) {
            $show_advanced_ui = false;
        }
    }

    $whitelist = $wpdb->get_col("SELECT `option_value` FROM $wpdb->options WHERE `option_name` LIKE '_whitelist%';");
    $blacklist_count = $wpdb->get_var("SELECT count(*) FROM $wpdb->options WHERE `option_name` LIKE '_blacklist%';");
    ?>
    <div class="wrap">
        <h1 class="fixrs-security-title"><span class="dashicons dashicons-vault"></span><?php _e('Website security', 'fixrs') ?></h1>
        
        <?php if ($success): ?>
        <div class="notice notice-success is-dismissible"><p><?= $success ?></p></div>
        <?php endif; ?>
        <?php if ($error): ?>
        <div class="notice notice-error is-dismissible"><p><?= $error ?></p></div>
        <?php endif; ?>
        
        <form id="fixrs-security-form" action="options-general.php?page=fixrs-security" method="post">
            <?php wp_nonce_field('fixrs-security-interface') ?>
            <input type="hidden" id="remove_list" name="remove_list" value="" />
            <input type="hidden" id="remove_ip" name="remove_ip" value="" />
            <table class="form-table">
                <tbody>
                    <tr>
                        <th scope="row"><?php _e('Your IP address', 'fixrs') ?></th>
                        <td>
                            <input type="text" value="<?php echo $_SERVER['REMOTE_ADDR'] ?>" readonly="readonly" name="my_ip" />
                            <button type="submit" name="what" value="add_my_ip_whitelist" class="button-primary"><?php _e('Add to whitelist', 'fixrs'); ?></button>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><?php _e('Whitelist', 'fixrs') ?></th>
                        <td>
                            <p><?php _e('The IP addresses on the whitelist will not be submitted to any security verification.', 'fixrs') ?></p>
                            <p>
                                <input type="text" placeholder="###.###.###.###" name="add_ip_whitelist" />
                                <button type="submit" name="what" value="add_ip_whitelist" class="button-primary"><?php _e('Add', 'fixrs'); ?></button>
                            </p>
                            <ul class="scroll-list">
                                <?php if (empty($whitelist)): ?>
                                    <li><?php _e('No IP addresses on the whitelist.', 'fixrs') ?></li>
                                <?php endif; ?>
                                <?php foreach ($whitelist as $wip): ?>
                                    <li>
                                        <code class="security-ip-list-item"><?php echo $wip; ?></code>
                                        <a href="javascript:void(0);" data-list="white" data-ip="<?php echo $wip ?>" class="security-list-remove-item dashicons dashicons-trash"></a></li>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><?php _e('Blacklist', 'fixrs') ?></th>
                        <td>
                            <p><?php _e('The IP addresses on the blacklist cannot log in at all, and are at risk of a complete ban of the website.', 'fixrs') ?></p>
                            <p><em><?php printf(__('There are currently %s IP addresses on the blacklist', 'fixrs'), '<b>' . $blacklist_count . '</b>') ?></em></p>
                            <p>
                                <input type="text" placeholder="###.###.###.###" name="search_ip_blacklist" id="search_ip_blacklist" />
                                <button type="button" class="button-secondary" name="search_blacklist" value="search_blacklist" id="search_blacklist" ><?php _e('Search the blacklist', 'fixrs') ?></button>
                                <span id="search_blacklist_code" class="dashicons"></span><i id="search_blacklist_reponse"></i>
                                <button type="submit" class="button-primary hide-if-js" name="what" value="delete_ip_blacklist" id="delete_search_blacklist"><?php _e('Remove from the blacklist', 'fixrs') ?></button>
                            </p>

                            <p>
                                <input type="text" placeholder="###.###.###.###" name="add_ip_blacklist" />
                                <button type="submit" name="what" value="add_ip_blacklist"  class="button-primary"><?php _e('Add', 'fixrs'); ?></button>
                            </p>
                        </td>
                    </tr>
                    <?php if ($show_advanced_ui): ?>
                        <tr>
                            <th scope="row"><?php _e('Advanced database operations', 'fixrs') ?></th>
                            <td>
                                <p><?php _e('Get fresh database backup before taking any action here.', 'fixrs') ?></p>

                                <?php if ($user_id_1_exists): ?>
                                    <div>
                                        <h2><?php _e('Reindex User IDs', 'fixrs') ?></h2>
                                        <p><?php printf(__('Users in database currently have sequential IDs starting by 1. This is a security risk, since bots can probe the websites by specifying <code>%s/?author=1</code>.', 'fixrs'), WP_HOME) ?></p>
                                        <p><button type="submit" class="button-primary" name="what" value="reindex_user_id" id="reindex_user_id"><?php _e('Reindex User IDs', 'fixrs') ?></button></p>
                                    </div>
                                <?php endif; ?>

                                <?php if ($users_with_compromized_usernames): ?>
                                    <div>
                                        <h2><?php _e('Fix Compromized User Nicenames', 'fixrs') ?></h2>
                                        <p><?php printf(_n('There are currently ONE user with nicename identical to its login. This name is accessible by probing the REST API or the Author page. Proceed to fix.', 'There are currently %s users with nicenames identical to their logins. These names are accessibles by probing the REST API or the Author pages. Proceed to fix.', count($users_with_compromized_usernames), 'fixrs'), count($users_with_compromized_usernames)) ?></p>
                                        <p><button type="submit" class="button-primary" name="what" value="rename_user_nicenames" id="rename_user_nicenames"><?php _e('Rename user nicenames', 'fixrs') ?></button></p>
                                    </div>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </form>
    </div>
    <?php
}

/**
 * 
 * LOGIN LIMIT
 * 
 */

/**
 * Get IP
 * 
 * Encode IP for stocking.
 * 
 * @since 1.0.0
 * @return string IP
 */
function get_ip($ip = false) {

    if (!$ip) {
        $ip = get_raw_ip();
    } else {
        $ip = filter_var($ip, FILTER_VALIDATE_IP);
        $ip = ($ip === false) ? '0.0.0.0' : $ip;
    }

    $_ip = '_IP_' . str_replace('.', '_', $ip);
    return $_ip;
}

/**
 * Get Raw IP
 * 
 * Get the User remote IP
 * 
 * @return type
 */
function get_raw_ip() {
    // Get client's IP address
    if (isset($_SERVER['HTTP_CLIENT_IP']) && array_key_exists('HTTP_CLIENT_IP', $_SERVER)) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
        $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
        $ips = array_map('trim', $ips);
        $ip = $ips[0];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'] ?? '0.0.0.0';
    }
    return $ip;
}

/**
 * Block user by IP, if it bypasses Apache
 */
function apply_gandalf_protocol() {
    $ip = get_ip();

    if (get_option('_gandalf' . $ip)) {
        wp_die(__('403 Forbidden. You are unauthorized.', 'fixrs'), __('403 Forbidden', 'fixrs'), 403);
    }
}

/**
 * Add IP to Whitelist
 * 
 * @since 1.0.2
 * @param IP Address $ip
 */
function add_to_whitelist($ip = false) {
    if ($ip === false) {
        $ip = get_raw_ip();
    }
    if (filter_var($ip, FILTER_VALIDATE_IP)) {
        $_ip = get_ip($ip);
        add_option('_whitelist' . $_ip, $ip, false);

        //clear useless options
        delete_option('_successful_login' . $_ip);
        delete_option('_blacklist' . $_ip);
        delete_option('_gandalf' . $_ip);
        delete_transient('four_oh_four' . $_ip);
        delete_transient('wait' . $_ip);
        delete_transient('attempt' . $_ip);
    }
}

/**
 * Remove IP from Whitelist
 *
 * @since 1.0.2
 * @param IP Address $ip
 */
function remove_from_whitelist($ip) {
    if (filter_var($ip, FILTER_VALIDATE_IP)) {
        delete_option('_whitelist' . get_ip($ip));
    }
}

/**
 * Is IP in whitelist
 * 
 * @param type $ip
 * @return boolean
 */
function is_whitelist($ip = false) {
    $whitelist = get_option('_whitelist' . get_ip($ip));
    if ($whitelist) {
        return true;
    }
    return false;
}

/**
 * 
 * Authenticate limit access
 * 
 * Run after WP official authentification procedure. 
 * If IP is blacklisted, blocs authentification whatever the result was.
 * Else, if authentification has failed, go through the attemps/wait/blacklist procedure.
 *  
 * @since 1.1.8
 * @param WP_User|WP_Error|null $user     WP_User or WP_Error object from a previous callback. Default null.
 * @param string                $username Username for authentication.
 * @param string                $password Password for authentication.
 * @return WP_User|WP_Error WP_User on success, WP_Error on failure.
 */
function authenticate_limit_access($user, $username, $password) {

    // if not trying to login pass through
    if (empty($username) && empty($password)) {
        return $user;
    }

    $_ip = get_ip();

    // blacklist
    $blacklist = get_option('_blacklist' . $_ip);
    if ($blacklist) {

        $blacklist++;
        update_option('_blacklist' . $_ip, $blacklist);

        if (FIXRS_SECURITY_MAX_BLACKLIST < $blacklist) {
            gandalf_protocol_add_ip();
        }
        return new \WP_Error('blacklisted', __('You are on the blacklist. Please contact the website administrators.', 'fixrs'));
    }

    // wait
    $wait_login_obj = get_transient('wait' . $_ip);

    $mss_bad_cred = __('Wrong ID or password.', 'fixrs');
    $mss_attemps = array(
        __('You have 4 out of 5 chances of signing in.', 'fixrs'),
        __('You have a 3 in 5 chance of signing in. <a href="%1$s">Forgot your password?</a>', 'fixrs'),
        __('You only have a 2 in 5 chance of signing in. We strongly suggest that you use the function <a href="%1$s">Forgot password?</a>', 'fixrs'),
        __('You only have one chance to log in. You will need to wait 5 minutes before trying again. <a href="%1$s"> Reset your password. </a>', 'fixrs'),
        __('You have reached the connection attempt limit. You must now wait 5 minutes before trying again. <a href="%1$s">Reset your password.</a>', 'fixrs')
    );
    $mss_delay = __('You had to wait up to %1$s before attempting to connect. You must now wait %2$s minutes (at %3$s) before attempting to connect.', 'fixrs');
    $mss_blacklist = __('You had to wait until %s before attempting to connect. You are now on the blacklist. Contact the site administrator.', 'fixrs');

    if ($wait_login_obj !== false) {

        // if it exists, it means that user should not have tried to log in, it was too soon.
        $time_to_login = $wait_login_obj['time'];

        switch ($wait_login_obj['delay']) {
            case 5:
                $new_delay = 15;
                break;
            case 15:
                $new_delay = 60;
                break;
            case 60:
                delete_transient('wait' . $_ip);
                // blacklist the son of a gun
                update_option('_blacklist' . $_ip, 1);
                security_log('login', 'new_blacklist');
                return new \WP_Error(
                        'blacklisted', '<strong>' . $mss_bad_cred . '</strong><br />'
                        . sprintf(
                                $mss_blacklist, date_i18n(_x('H\hi', 'Hour and minute', 'fixrs'), $time_to_login)
                        )
                );
        }
        $new_time_to_login = $time_to_login + ( $new_delay * MINUTE_IN_SECONDS );

        set_transient('wait' . $_ip, array('delay' => $new_delay, 'time' => $new_time_to_login), $new_delay * MINUTE_IN_SECONDS);
        security_log('login', 'wait_' . $new_delay);
        return new \WP_Error(
                'wait_' . $new_delay, '<strong>' . $mss_bad_cred . '</strong><br />'
                . sprintf(
                        $mss_delay, date_i18n(_x('H\hi', 'Hour and minute', 'fixrs'), $time_to_login), $new_delay, date_i18n(_x('H\hi', 'Hour and minute', 'fixrs'), $new_time_to_login)
                )
        );
    }

    // LOGIN SUCCESSFUL
    // if already a WP_User, signon was a success, passthrough
    if (is_a($user, 'WP_User')) {
        // delete transients
        delete_transient('attempt' . $_ip);
        delete_transient('wait' . $_ip);

        // log in successful login to whitelist IP
        $hashed_username = md5($user->user_login);
        $successful_login = get_option('_successful_login' . $_ip, array());
        if (!in_array($hashed_username, $successful_login)) {
            $successful_login[] = $hashed_username;
            update_option('_successful_login' . $_ip, $successful_login);
        }

        if (count($successful_login) >= FIXRS_SECURITY_LOGIN_TO_WHITELIST) {
            add_to_whitelist();
        }

        return $user;
    }


    // LOGIN FAILED
    // the login failed for some reason, start the Attempts procedure
    $attempts = intval(get_transient('attempt' . $_ip));

    if ($attempts === 4) {
        delete_transient('attempt' . $_ip);
        set_transient('wait' . $_ip, array('delay' => 5, 'time' => time() + ( 5 * MINUTE_IN_SECONDS )), 5 * MINUTE_IN_SECONDS);
    } else {
        set_transient('attempt' . $_ip, $attempts + 1, HOUR_IN_SECONDS);
    }

    security_log('login', 'attempt ' . $attempts);
    return new \WP_Error(
            'attempt', '<strong>' . $mss_bad_cred . '</strong><br />'
            . sprintf(
                    $mss_attemps[$attempts], wp_lostpassword_url()
            )
    );
}

/**
 * Add Fixrs-core error messages to login Shake
 * 
 * @since 1.1.8
 * @param array $shake_error_codes
 * @return array
 */
function add_shake_error_codes($shake_error_codes) {
    $shake_error_codes = array_merge($shake_error_codes, array('wait_5', 'wait_15', 'wait_60', 'blacklisted', 'attempt'));
    return $shake_error_codes;
}

add_filter('shake_error_codes', 'Fixrs\security\add_shake_error_codes');

/**
 * security 404
 * 
 * Checks and manages 404 errors for possible attacks
 * 
 * @global object $wpdb
 */
function security_404() {

    if (is_main_query() && is_404()) {

        //not for media
        $uri = $_SERVER['REQUEST_URI'];
        $allowed_mime_types = implode('|', array_keys(get_allowed_mime_types()));
        if (preg_match('/^.*\.(' . $allowed_mime_types . ')$/i', $uri)) {
            wp_die(sprintf(__('File “%s” missing.', 'fixrs'), $uri), sprintf(__('File “%s” missing.', 'fixrs'), $uri), array('response' => 404));
        }

        // not for legitimate pages
        global $wpdb;
        // by id?
        if (preg_match('/(\?|&)p=(\d+)/', $uri, $matches)) {
            if (false !== get_post_status(intval($matches[2]))) {
                return;
            }
        }
        // by slug?
        if (preg_match('/\/([\w-%]+)\/$/', $uri, $matches)) {
            $post_name = esc_sql($matches[1]);
            $sql = "SELECT ID, post_name, post_status, post_type
                    FROM $wpdb->posts
                    WHERE post_name IN (%s)";

            $check = $wpdb->get_results($wpdb->prepare($sql, $post_name));
            if (!empty($check)) {
                return;
            }
        }


        $_ip = get_ip();
        $blacklist = get_option('_blacklist' . $_ip);
        $four_oh_four = get_transient('four_oh_four' . $_ip);
        if ($blacklist) {
            // add blacklist
            $blacklist++;
            update_option('_blacklist' . $_ip, $blacklist);
            if (FIXRS_SECURITY_MAX_BLACKLIST < $blacklist) {
                gandalf_protocol_add_ip();
            }
        } elseif ($four_oh_four > FIXRS_SECURITY_MAX_404) {
            //blacklist
            delete_transient('four_oh_four' . $_ip);
            update_option('_blacklist' . $_ip, 1);
            return;
        } else {
            $four_oh_four++;
            set_transient('four_oh_four' . $_ip, $four_oh_four, 20 * 60);
        }
        security_log('four_oh_four');
    }
}

/**
 * Gandalf protocol Add IP
 * 
 * Add IP to the Gandalf queue
 * 
 * @since 1.0.2
 * @param IP address $ip
 * @uses function gandalf_protocol_write_ips()
 * @return bool Success|Failure
 */
function gandalf_protocol_add_ip($ip = false) {

    if (!$ip) {
        $ip = get_raw_ip();
    }

    $htaccess = ABSPATH . '.htaccess';
    if (is_writable($htaccess)) {

        // add the ip to gandalf queue option
        $queue = get_option('fixrs_gandalf_queue', []);

        if (!in_array($ip, $queue)) {
            $queue[] = $ip;

            if (count($queue) < 5) {
                gandalf_protocol_write_ips($queue);
                $queue = array();
            }
            update_option('fixrs_gandalf_queue', $queue);
        }
    }
    // update that option
    update_option('_gandalf' . get_ip($ip), 1);

    return true;
}

/**
 * Gandalf protocol Write IPs
 * 
 * Write IP to deny list of htaccess.
 * 
 * @param array $ips
 * @return bool Success|Failure
 */
function gandalf_protocol_write_ips($ips = array()) {

    if (empty($ips)) {
        return false;
    }

    $htaccess = ABSPATH . '.htaccess';
    if (is_writable($htaccess)) {
        require_once ABSPATH . 'wp-admin/includes/misc.php';

        $gandalf_list = extract_from_markers($htaccess, 'GANDALF') ?: [];

        if (count($gandalf_list)) {
            $old_gandalf_denies = array_slice($gandalf_list, 2, count($gandalf_list) - 4);
        }

        $new_denies = array();
        for ($i = 0; $i < count($ips); $i++) {
            $new_denies[] = 'deny from ' . $ips[$i];
        }

        $update_gandalf_denies = array_unique(array_merge($new_denies, $old_gandalf_denies));

        $beginning = array(
            '<Limit GET POST>',
            'order allow,deny'
        );
        $end = array(
            'allow from all',
            '</Limit>'
        );

        $new_gandalf_list = array_merge($beginning, $update_gandalf_denies, $end);
        insert_with_markers($htaccess, 'GANDALF', $new_gandalf_list);
    }
    return true;
}

/**
 * Gandalf protocol Remove IP
 * 
 * Remove IP address from htaccess file, and all options and transients
 * 
 * @param IP Address $ip
 * @return bool Success|Failure
 */
function gandalf_protocol_remove_ip($ip = false) {

    if (!$ip) {
        return false;
    }

    $_ip = get_ip($ip);

    delete_transient('four_oh_four' . $_ip);
    delete_transient('wait' . $_ip);
    delete_transient('attempt' . $_ip);
    delete_option('_blacklist' . $_ip);
    delete_option('_gandalf' . $_ip);

    $htaccess = ABSPATH . '.htaccess';
    if (is_writable($htaccess)) {
        require_once ABSPATH . 'wp-admin/includes/misc.php';

        $gandalf_list = extract_from_markers($htaccess, 'GANDALF');
        $gandalf_denies = array();

        if (count($gandalf_list)) {
            $gandalf_denies = array_slice($gandalf_list, 2, count($gandalf_list) - 4);
        }

        foreach ($gandalf_denies as $key => $deny) {
            if (($key = array_search('deny from ' . $ip, $gandalf_denies)) !== false) {
                unset($gandalf_denies[$key]);
            }
        }

        $beginning = array(
            '<Limit GET POST>',
            'order allow,deny'
        );
        $end = array(
            'allow from all',
            '</Limit>'
        );
        if (!empty($gandalf_denies)) {
            $new_gandalf_list = array_merge($beginning, $gandalf_denies, $end);
        } else {
            $new_gandalf_list = array();
        }
        insert_with_markers($htaccess, 'GANDALF', $new_gandalf_list);
    }

    return true;
}

/**
 * Write security log
 * 
 * Write data to security log
 * 
 * @since 1.0.0
 * @param string $log Type of entry (four_oh_four, login, etc.)
 * @param string|array $data The actual data to write
 * @return bool Success|Failure
 */
function security_log($log, $data = false) {

    $security_log = WP_CONTENT_DIR . '/security.log';

    if (empty($log) || !is_writable($security_log)) {
        return false;
    }

    $ip = get_raw_ip();

    $array_log = array();

    $array_log = array($log);

    $array_log[] = '[' . date_i18n('c') . ']';

    $array_log[] = '[' . $ip . ']';

    if (!( $geo_info = get_transient('geo_info' . get_ip()) )) {
        $timeout = stream_context_create(['http' => ['timeout' => 10]]);
        $return = file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $ip, false, $timeout);
        if ($return) {
            $geo_info = unserialize($return);
            if ($geo_info) {
                set_transient('geo_info' . get_ip(), $geo_info, DAY_IN_SECONDS);
            }
        }
    }

    $array_log[] = html_entity_decode('['
            . $geo_info['geoplugin_city']
            . ','
            . $geo_info['geoplugin_regionName']
            . ','
            . $geo_info['geoplugin_countryName']
            . ']', ENT_QUOTES, "utf-8");

    if ($log == 'four_oh_four') {
        $array_log[] = '[URI=' . $_SERVER['REQUEST_URI'] . ']';
    }

    if (is_array($data)) {
        $array_log = array_merge($array_log, $data);
    } elseif (false !== $data) {
        $array_log[] = $data;
    }

    error_log(implode(' ', $array_log) . "\n", 3, $security_log);
    return true;
}
