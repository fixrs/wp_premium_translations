<?php

/*
  Copyright (C) 2020 fixrs

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
namespace Fixrs\ACF;

defined('ABSPATH') || die('Invalid request.');

/**
 * Create directory
 */
function create_acf_json_folder() {
    if (!file_exists(WP_CONTENT_DIR . '/acf-json')) {
        mkdir(WP_CONTENT_DIR . '/acf-json');
    }
}

add_action('Fixrs/activate_plugin', 'Fixrs\ACF\create_acf_json_folder');

add_filter('acf/settings/save_json', 'Fixrs\ACF\save_acf_json_folder');
add_filter('acf/settings/load_json', 'Fixrs\ACF\load_acf_json_folder');

/**
 * Sauvegarde les acf-json dans un sous-dossier de WP-CONTENT.
 * 
 * @return String
 */
function save_acf_json_folder() {
    return WP_CONTENT_DIR . '/acf-json';
}

/**
 * Utilise les acf-json de bases et ceux personnalisés.
 *
 * Cela nous permet d'avoir les champs du parent et du child en même temps.
 * 
 * @param Array $paths all the paths to load
 * @return Array
 */
function load_acf_json_folder($paths) {
    $paths = array();

    //default JSON
    $paths[] = FIXRSCORE_PATH . '/acf-json';

    //custom JSON
    $paths[] = WP_CONTENT_DIR . '/acf-json';

    return $paths;
}

/**
 * Add an option page
 * 
 * @return type
 */
function add_acf_option_page() {

    if (function_exists('acf_add_options_page')) {
        acf_add_options_page();
    }

    return;
}

add_action('plugins_loaded', 'Fixrs\ACF\add_acf_option_page', 10, 1);


