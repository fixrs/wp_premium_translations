<?php

// Exit if accessed directly
if (!defined('ABSPATH')) {
	exit;
}

/**
 * Vérifie que c’est un membre de Fixrs qui exécute une requête
 * 
 * @todo rafiner le test
 * 
 * @return bool
 */
function is_dev() {
    if (defined('THIS_IS_FIXRS') ){
        return THIS_IS_FIXRS;
    }
    $test = false;
	return $test;
}

/**
 * Debug
 * 
 * @param any $val La valeur à deboguer
 * @param bool $dump Echo un var_dump ou un print_r
 * @param bool $echo Echo ou retourne le debug
 */
function debug($val, $dump = false, $echo = true) {
	if (is_dev()) {
		$debug = '';
		$debug .= '<pre class="debug">';
		if ($dump) {
			$debug .= var_export($val, true);
		} else {
			$debug .= print_r($val, true);
		}
		$debug .= '</pre>';
		if ($echo) {
			echo $debug;
		} else {
			return $debug;
		}
	}
}

/**
 * Ecrit la valeur $val dans le fichier d’error de PHP ou dans debug.log
 * 
 * @param any $var La valeur à deboguer
 * @param bool $dump Echo un var_dump ou un print_r
 */
function debug_log($val, $dump = false) {
	if (is_dev()) {
		$debug = debug($val,$dump,false);
		error_log( $debug );
	}
}


function fixrs_activate_demo() {
    if (isset($_GET['DEMO'])) {
        setcookie('fixrs_show_demo', 1,0,'/');
    }

    return;
}

add_action('plugins_loaded', 'fixrs_activate_demo', 1, 1);