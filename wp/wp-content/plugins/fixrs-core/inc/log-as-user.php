<?php

namespace Fixrs\log_as_user;

// Exit if accessed directly
if (!defined('ABSPATH')) {
    exit;
}

/**
 * Connect as user
 */
function add_temp_password($profile_user) {
    
    if (current_user_can('manage_options')):
        ?>
        <h2><?php _e('User account takeover', 'fixrs') ?></h2>
        <p><?php _e('Define a temporary password to take over the user account without changing it’s real password.', 'fixrs') ?></p>
        <table class="form-table" role="presentation">
            <tbody>
                <tr class="user-url-wrap">
                    <th><label for="temp-password-user"><?= __('Temporary password', 'fixrs') ?></label></th>
                    <td><input type="text" name="temp-password-user" id="temp-password-user" value="" /> <?php _e('Password will last 5 minutes, and can be only used once.', 'fixrs') ?></td>
                </tr>
            </tbody>
        </table>
        <?php
    endif;
}

add_action('edit_user_profile', 'Fixrs\log_as_user\add_temp_password', 10, 1);


function save_temp_password($user_id,$old_user_data) {
    
    if (current_user_can('manage_options')):
        
        $temp_password = filter_input(INPUT_POST,'temp-password-user');
    
        if ($temp_password){
            set_transient('temp_password_'.$user_id, $temp_password, 5 * MINUTE_IN_SECONDS);
        }
        
    endif;
}

add_action('profile_update','Fixrs\log_as_user\save_temp_password',10,2);

/**
 * Authenticate User with temporary password
 * 
 * @param type $user
 * @param type $username
 * @param type $password
 * @return type
 */
function authenticate($user, $username, $password) {

    // if user not authenticated, test for temp password
    if (!is_a($user,'WP_User')) {
        $possible_user = get_user_by('login',$username);
        if (!is_a($possible_user,'WP_User')){
            return $user;
        }
        $temp_password = get_transient('temp_password_' . $possible_user->ID );
        if ($password === $temp_password){
            $user = $possible_user;
            delete_transient('temp_password_' . $possible_user->ID);
        }
    }
    return $user;
}

add_filter('authenticate', 'Fixrs\log_as_user\authenticate', 5, 3);
