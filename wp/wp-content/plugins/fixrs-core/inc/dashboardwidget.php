<?php

namespace Fixrs\dashboardwidget;

// Exit if accessed directly
if (!defined('ABSPATH')) {
    exit;
}

/**
 * Add a widget to the dashboard.
 *
 * This function is hooked into the 'wp_dashboard_setup' action below.
 * 
 */
function add_dashboard_widgets() {
	global $wp_meta_boxes;

    if (!FIXRS_WHITELABEL){
    
        wp_add_dashboard_widget(
                'fixrs_support_dashboard_widget', // Widget slug.
                __('Support and Development by Fixrs', 'fixrs'), // Title.
                'Fixrs\dashboardwidget\support_dashboard_widget_function' // Display function.
        );

    }

    remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
    //remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
    //remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');//since 3.8

}

add_action('wp_dashboard_setup', 'Fixrs\dashboardwidget\add_dashboard_widgets');


/**
 * Create the function to output the contents of our Dashboard Widget.
 */
function support_dashboard_widget_function() {
    ?>
    <p><?= __('The Fixrs team is always there to assist you.', 'fixrs'); ?></p>
	<p>
        <?= sprintf( __('<a href="mailto:emergency@fixrs.ca">By email</a>', 'fixrs')) ?>
        —
        <?= sprintf( __('<a href="https://3.basecamp.com/4452551/projects/" target="_blank">On Basecamp</a>', 'fixrs')) ?>
    </p>
    <?php
    if (FIXRS_ACTIVATE_DEMO){
        if (isset($_COOKIE['fixrs_show_demo'])):?>
        <p><?php _e('Demo features activated.', 'fixrs') ?></p>
        <?php else: ?>
        <p><?php _e('We are working on a feature and you don’t see it?', 'fixrs') ?> <a href="<?= admin_url('/?DEMO='.wp_hash(FIXRSCORE_PATH)) ?>" class="button-secondary"><?php _e('Activate it for this session', 'fixrs') ?></a></p>
        <?php endif; ?>
    <?php }
}

/**
 * Retire le tagline de l’admin de WP
 */
add_filter('admin_footer_text', '__return_false');