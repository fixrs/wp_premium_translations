'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require("gulp-rename");
var cssnano = require('cssnano');
var autoprefixer = require('autoprefixer');
var notify = require("gulp-notify");
var postcss = require('gulp-postcss');
var sourcemaps = require('gulp-sourcemaps');
var terser = require('gulp-terser');
var babel = require('gulp-babel');
var pump = require('pump');
var converter = require('sass-convert');
var sassdoc = require('sassdoc');

var reportError = function (error) {
	notify({
		title: 'Gulp Task Error',
		message: 'Line ' + error.line + "\n" + error.formatted
	}).write(error);

	this.emit('end');
}

// Compile sass files in scss folder.
gulp.task('sass', function (err) {
	pump([
		gulp.src([
			'./dev/scss/templates/**/*.scss',
		]),
		sass.sync().on('error', reportError),
		postcss([autoprefixer()]),
		gulp.dest('./templates')

	]);

	pump([
		gulp.src([
			'./dev/scss/**/*.scss',
			'!./dev/scss/templates/**/*.scss'
		]),
		sourcemaps.init(),
		sass.sync().on('error', reportError),
		postcss([autoprefixer()]),
		sourcemaps.write('./maps'),
		gulp.dest('./dev/css'),
	], err);
});

// minify CSS
gulp.task('minify:css', function (err) {
	return gulp.src([
		'./dev/scss/**/*.scss',
		'!./dev/scss/templates/**/*.scss'
	])
			.pipe(sourcemaps.init())
			.pipe(sass().on('error', reportError))
			.pipe(postcss([
				autoprefixer(),
				cssnano()
			]))
			.pipe(rename({
				suffix: '.min'
			}))
			.pipe(sourcemaps.write('./maps'))
			.pipe(gulp.dest('./css'));
});

// Minify JavaScript
gulp.task('minify:js', function (err) {
	return gulp.src([
		'./dev/js/**/*.js',
		'!./dev/js/**/*min.js'
	])
			.pipe(sourcemaps.init())
			.pipe(babel())
			.pipe(terser())
			.pipe(rename({
				suffix: '.min'
			}))
			.pipe(sourcemaps.write('./maps'))
			.pipe(gulp.dest('./js'));
});

// watch for sass file change and compile on save.
gulp.task('sass:watch', gulp.series(['sass', 'minify:css'], function () {
	gulp.watch('./dev/scss/**/*.scss', gulp.series(['sass', 'minify:css']));
}));

// watch for js file change and compile on save.
gulp.task('js:watch', gulp.series(['minify:js'], function () {
	gulp.watch([
		'./dev/js/**/*.js',
		'!./dev/js/**/*min.js'
	], gulp.series(['minify:js']));
}));

// Watch SASS and JS files for changes and compile them.
gulp.task('all:watch', gulp.parallel(['sass:watch', 'js:watch']));

// Minify all files
gulp.task('minify', gulp.parallel(['minify:css', 'minify:js']));

// Execute everything needed for the live version
gulp.task('compile', gulp.series(['minify']));

// generate SASS documentation
gulp.task('sassdoc', function () {
	var options = {
		dest: 'docs/sass',
		verbose: true,
		display: {
			access: ['public', 'private'],
			alias: true,
			watermark: false,
		},
		theme: "doc-templates/sassdoc/index.js"
	};

	pump([
		gulp.src('./dev/scss/*.+(sass|scss)'),
		converter({
			from: 'sass',
			to: 'scss',
		}),
		sassdoc(options)
	]);
});


// Generate all the documentations.
gulp.task('generatedoc', gulp.series(['sassdoc']));